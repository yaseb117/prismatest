//
//  Serialize.swift
//  PrismaTest
//
//  Created by sebai yassine on 03/05/2020.
//  Copyright © 2020 sebai yassine. All rights reserved.
//

import UIKit

extension Formatter {
    static let iso8601: DateFormatter = {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.dateFormat = "yyyy-dd-MM HH:mm:ss"
        return formatter
    }()
}

protocol JSONCodable: Codable {
    typealias JSON = [String : Any]
    func toDictionary() -> JSON?
    init?(json: Any)
}

extension JSONCodable {
    func toDictionary() -> JSON? {
        // Encode the data
        if let jsonData = try? JSONEncoder().encode(self),
            // Create a dictionary from the data
            let dict = try? JSONSerialization.jsonObject(with: jsonData, options: []) as? JSON {
            return dict
        }
        return nil
    }
    
    init?(json: Any) {
        do {
            let data = try JSONSerialization.data(withJSONObject: json, options: [])
            let decoder =  JSONDecoder()
            decoder.dateDecodingStrategy = .formatted(Formatter.iso8601)
            let object = try decoder.decode(Self.self, from: data)
            self = object
        } catch  {
            return nil
        }
    }
}
