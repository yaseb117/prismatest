//
//  DataModel.swift
//  PrismaTest
//
//  Created by sebai yassine on 03/05/2020.
//  Copyright © 2020 sebai yassine. All rights reserved.
//

import UIKit



struct UserInformation: JSONCodable {
    var name: String?
    
    enum CodingKeys : String, CodingKey {
        case name
        case data
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let data = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .data)
        self.name = try data.decode(String.self, forKey: .name)
    }
    
    func encode(to encoder: Encoder) throws { }
}

