//
//  UserInformationWebServiceManager.swift
//  PrismaTest
//
//  Created by sebai yassine on 03/05/2020.
//  Copyright © 2020 sebai yassine. All rights reserved.
//

import Foundation
import Alamofire


protocol UserInformationWebServiceManagerProtocol {
    func getUserInformation(completion: @escaping (_ result: Any?) -> (), failure: @escaping(_ error: APIError?) -> ())
}

public class UserInformationWebServiceManager: UserInformationWebServiceManagerProtocol {
    
    func getUserInformation(completion: @escaping (Any?) -> (), failure: @escaping (APIError?) -> ()) {
        let endPoint: UserInformationEndpoint = .GetUserInformation
        let router = UserInformationRouter(endpoint: endPoint)
        
        Alamofire.request(router).validate().responseJSON { (respnse) in
            switch respnse.result {
            case .success(let value):
                completion(value)
            case .failure(let error):
                if let error = error as? AFError {
                    failure(error.apiError)
                } else {
                    failure(.timeOut)
                }            }
        }
    }
}

