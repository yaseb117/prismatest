//
//  ApiError.swift
//  PrismaTest
//
//  Created by sebai yassine on 03/05/2020.
//  Copyright © 2020 sebai yassine. All rights reserved.
//

import Foundation
import Alamofire

enum APIError: Error {
    case invalidJSON
    case unknownError
    case connectionError
    case invalidCredentials
    case invalidRequest
    case notFound
    case invalidResponse
    case serverError
    case serverUnavailable
    case timeOut
    case unsuppotedURL
    case unauthorized
    case noData
    
    var localizedDescription: String {
        switch self {
        case .invalidJSON:
            return Constant.error.invalidJSON
        case .timeOut, .connectionError:
            return Constant.error.connectionError
        case .noData:
            return "pas de données à afficher"
        default:
            return ""
        }
    }
}

extension AFError {
    var apiError: APIError {
        switch self.responseCode {
        case 400:
            return .invalidJSON
        case 401:
            return .unauthorized
        case 402:
            return .invalidCredentials
        default:
            return .unknownError
        }
    }
}

