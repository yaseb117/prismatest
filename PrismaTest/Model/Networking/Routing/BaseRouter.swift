//
//  BaseRouter.swift
//  PrismaTest
//
//  Created by sebai yassine on 03/05/2020.
//  Copyright © 2020 sebai yassine. All rights reserved.
//

import UIKit
import Alamofire


public typealias JSONDictionary = [String: AnyObject]
typealias APIParams = [String : AnyObject]?

protocol APIConfiguration {
    var method: Alamofire.HTTPMethod  { get }
    var encoding: Alamofire.ParameterEncoding? { get }
    var path: String { get }
    var parameters: APIParams { get }
    var baseUrl: String { get }
}
    
class BaseRouter : URLRequestConvertible, APIConfiguration {
    
    init() {}
    
    var method: Alamofire.HTTPMethod  {
        
        fatalError("Subclasses need to implement the Alamofire.Method var in subclass.")
    }
    
    var encoding: Alamofire.ParameterEncoding? {
        fatalError("Subclasses need to implement the Alamofire.ParameterEncoding var in subclass.")
    }
    
    var path: String {
        fatalError("Subclasses need to implement the path var in subclass.")
    }
    
    var parameters: APIParams {
        fatalError("Subclasses need to implement the paramets var in subclass.")
    }
    
    var baseUrl: String {
        let baseUrl = Constant.Api.baseURL
        return baseUrl
    }
    
    
    func asURLRequest() throws -> URLRequest {
        let url = URL(string: baseUrl)!
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        
        /*
         if let token = Router.OAuthToken {
         urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
         }*/
        
        switch self {
        default:
            return urlRequest
        }
    }
    
}



