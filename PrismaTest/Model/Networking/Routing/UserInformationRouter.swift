//
//  UserInformationRouter.swift
//  PrismaTest
//
//  Created by sebai yassine on 03/05/2020.
//  Copyright © 2020 sebai yassine. All rights reserved.
//

import Foundation
import Alamofire

enum UserInformationEndpoint {
    case GetUserInformation
}

class UserInformationRouter : BaseRouter {
    
    var endpoint: UserInformationEndpoint
    init(endpoint: UserInformationEndpoint) {
        self.endpoint = endpoint
    }
    
    override var method: Alamofire.HTTPMethod{
        
        switch endpoint {
        case .GetUserInformation: return .get
            
        }
    }
    
    override var path: String {
        switch endpoint {
        case .GetUserInformation: return Constant.EndPoint.getUserInformation
        }
    }
    
    override var parameters: APIParams {
        switch endpoint {
        case .GetUserInformation:
            return [:]
        }
    }
    
    override var encoding: Alamofire.ParameterEncoding? {
        switch endpoint {
            
        case .GetUserInformation: return JSONEncoding.default
            
        }
    }
}

