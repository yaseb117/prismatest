//
//  UserInformationService.swift
//  PrismaTest
//
//  Created by sebai yassine on 03/05/2020.
//  Copyright © 2020 sebai yassine. All rights reserved.
//

import Foundation

protocol UserInformationServiceProtocol {
    func getUserInformation(completion: @escaping (_ result: UserInformation?) -> (), failure: @escaping(_ error: APIError?) -> ())
}

class UserInformationService: UserInformationServiceProtocol {
    
    let userInformationWebServiceManager : UserInformationWebServiceManagerProtocol
    
    init(userInformationWebServiceManager : UserInformationWebServiceManagerProtocol) {
        self.userInformationWebServiceManager = userInformationWebServiceManager
    }
    
    func getUserInformation(completion: @escaping (UserInformation?) -> (), failure: @escaping (APIError?) -> ()) {
        self.userInformationWebServiceManager.getUserInformation(completion: { (result) in
            if let jsonString = result  {
            
                guard let result = UserInformation(json: jsonString) else {
                    failure(.invalidJSON)
                    return
                }
                completion(result)
            }
            else {
                let dummyError = APIError.invalidJSON
                failure(dummyError)
            }
        }) { (error) in
            
        }
    }
    

}
