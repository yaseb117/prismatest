//
//  UIViewController+Alert.swift
//  PrismaTest
//
//  Created by sebai yassine on 03/05/2020.
//  Copyright © 2020 sebai yassine. All rights reserved.
//

import UIKit

typealias SelectedBlock = (Int) -> Void

extension UIViewController {
    
    func presentAlertController(withTitle title:String, message: String, options: [String], selected: SelectedBlock? ) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        for (index, option) in options.enumerated() {
            let action = UIAlertAction(title: option, style: .default, handler: { (action) in
                if let block = selected {
                    block(index)
                }
            })
            
            alert.addAction(action)
        }
        
        present(alert, animated: true, completion: nil)
    }
    
}

