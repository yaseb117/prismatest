//
//  UIImageView+Designable.swift
//  PrismaTest
//
//  Created by sebai yassine on 03/05/2020.
//  Copyright © 2020 sebai yassine. All rights reserved.
//

import UIKit

@IBDesignable
class PTImageView: UIImageView {

    @IBInspectable var borderColor: UIColor = UIColor.clear {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
        
    @IBInspectable var borderWidth: CGFloat = 1.0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
        
    @IBInspectable var cornerRadius: CGFloat = 1.0 {
        didSet {
            layer.cornerRadius = cornerRadius
            clipsToBounds = true
        }
    }

    //MARK: Initializers
    
    override init(frame : CGRect) {
        super.init(frame : frame)
        configure()
    }
    
    convenience init() {
        self.init(frame:CGRect.zero)
        configure()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configure()
    }
    
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        configure()
    }
    
    override public func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        configure()
    }
    
    func configure() {
        layer.borderColor = borderColor.cgColor
        layer.borderWidth = borderWidth
        layer.cornerRadius = cornerRadius
    }

    
    override public func layoutSubviews() {
        super.layoutSubviews()
    }
        
}

