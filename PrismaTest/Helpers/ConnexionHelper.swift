//
//  ConnexionHelper.swift
//  PrismaTest
//
//  Created by sebai yassine on 03/05/2020.
//  Copyright © 2020 sebai yassine. All rights reserved.
//

import Foundation
import Reachability

class ConnexionHelper {
    class func verifyConnection(withSuccessBlock success: @escaping () -> Void, failure: @escaping () -> Void) {
        let reachability = Reachability()!
        if reachability.connection != .none {
            success()
        } else {
            failure()
        }
    }
}

