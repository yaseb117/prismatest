//
//  Constant.swift
//  PrismaTest
//
//  Created by sebai yassine on 03/05/2020.
//  Copyright © 2020 sebai yassine. All rights reserved.
//

import UIKit

struct Constant {
    struct Api {
        static let baseURL = "https://reqres.in/"
    }
    
    struct EndPoint {
        static let getUserInformation = "api/user/1"
    }
    
    struct segue {
        static let updateProfileSegue = "updateProfileSegue"
    }
    
    struct error {
        static let invalidJSON =  "Probléme de parsing"
        static let timeOut = "Verfier votre conextion internet "
        static let connectionError = "Verfier votre conextion internet "
        static let noData = "pas de données à afficher"
    }
    
    struct localizedString {
        static let warning = "Atention!"
        static let ok = "OK"
    }
}
