//
//  UpdateProfileViewController.swift
//  PrismaTest
//
//  Created by sebai yassine on 03/05/2020.
//  Copyright © 2020 sebai yassine. All rights reserved.
//

import UIKit

protocol UpdateProfileDelegate: class {
    func didUpdateProfile(with fullName: String?)
}

class UpdateProfileViewController: UIViewController {

    //MARK: IBOutlets
    
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    
    //MARK: Propreties
    
    weak var delegate: UpdateProfileDelegate?
    
    //MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK: IBActions
    
    @IBAction func submit(_ sender: Any) {
        let fullname = "\(self.firstNameTextField.text!) \(self.lastNameTextField.text!)"
        self.dismiss(animated: true) {
            self.delegate?.didUpdateProfile(with: fullname)
        }
    }
    
}
