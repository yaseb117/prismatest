//
//  ViewController.swift
//  PrismaTest
//
//  Created by sebai yassine on 03/05/2020.
//  Copyright © 2020 sebai yassine. All rights reserved.
//

import UIKit
import MBProgressHUD

class ProfileViewController: UIViewController {

    //MARK: IBOutlets
    
    @IBOutlet weak var nameLabel: UILabel!
    
    //MARK: Propreties
    
    private var apiService: UserInformationServiceProtocol!
    
    //MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.apiService = UserInformationService(userInformationWebServiceManager: UserInformationWebServiceManager())
        self.getProfile()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.getProfile()
    }

    //MARK: Private
    
    private func getProfile() {
        ConnexionHelper.verifyConnection(withSuccessBlock: { [weak self] in
            guard let `self` = self else { return }
            MBProgressHUD.showAdded(to: self.view, animated: true)
            self.apiService.getUserInformation(completion: { [weak self] (profile) in
                guard let `self` = self else { return }
                MBProgressHUD.hide(for: self.view, animated: true)
                self.nameLabel.text = profile?.name
            }) { [weak self] (error) in
                guard let `self` = self else { return }
                MBProgressHUD.hide(for: self.view, animated: true)
                self.presentAlertController(withTitle: Constant.localizedString.warning, message: error?.localizedDescription ?? "", options: [Constant.localizedString.ok], selected: nil)
            }
        }) {  [weak self] in
            guard let `self` = self else { return }
            let error = APIError.connectionError
            self.presentAlertController(withTitle: Constant.localizedString.warning, message: error.localizedDescription, options: [Constant.localizedString.ok], selected: nil)
        }
    }
}

//MARK: - Navigation

extension ProfileViewController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constant.segue.updateProfileSegue {
            let vc = segue.destination as! UpdateProfileViewController
            vc.delegate = self
        }
    }
}

//MARK: - UpdateProfileDelegate

extension ProfileViewController: UpdateProfileDelegate {
    func didUpdateProfile(with fullName: String?) {
        self.nameLabel.text = fullName
    }
}
